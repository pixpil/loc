#
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: event/global/Ch1/CH_FakeJohn.sq:10
msgctxt "<talk> @CH_FakeJohn"
msgid "约翰大师，您一定能领到有很多吧。"
msgstr "约翰大师，您一定能领到有很多吧。"

#: event/global/Ch1/CH_FakeJohn.sq:19
msgctxt "<talk> @CH_FakeJohn"
msgid "什么时候才能，住上大师您那样的大房子？"
msgstr "什么时候才能，住上大师您那样的大房子？"

#: event/global/Ch1/CH_FakeJohn.sq:24
msgctxt "<talk> @CH_Yocar"
msgid "100年后，你的努力值就能达到180。"
msgstr "100年后，你的努力值就能达到180。"

#: event/global/Ch1/CH_FakeJohn.sq:31
msgctxt "<talk> @CH_FakeJohn"
msgid "{~}闭嘴！没有问你！"
msgstr "{~}闭嘴！没有问你！"

#: event/global/Ch1/CH_FakeJohn.sq:41
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"小约。\n"
"{====}我的新房子怎么样？\n"
"{====}要不要进来坐坐？"
msgstr ""
"小约。\n"
"{====}我的新房子怎么样？\n"
"{====}要不要进来坐坐？"

#: event/global/Ch1/CH_FakeJohn.sq:48
msgctxt "<talk> @CH_FakeJohn"
msgid "欧，等下。"
msgstr "欧，等下。"

#: event/global/Ch1/CH_FakeJohn.sq:52
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"镇长好像说过，不能让你进这屋子。\n"
"{====}哈哈哈。"
msgstr ""
"镇长好像说过，不能让你进这屋子。\n"
"{====}哈哈哈。"

#: event/global/Ch1/CH_FakeJohn.sq:61
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"不要看啦，\n"
"{====}我是不会让你进去的。"
msgstr ""
"不要看啦，\n"
"{====}我是不会让你进去的。"

#: event/global/Ch1/CH_FakeJohn.sq:74
msgctxt "<talk> @CH_FakeJohn"
msgid "这下糟糕了，居然被他找到了。"
msgstr "这下糟糕了，居然被他找到了。"

#: event/global/Ch1/CH_FakeJohn.sq:79
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"约...约翰大师，\n"
"{====}您是在哪找到的，第五卷？\n"
"{====}没别的意思，我...随便问问。"
msgstr ""
"约...约翰大师，\n"
"{====}您是在哪找到的，第五卷？\n"
"{====}没别的意思，我...随便问问。"

#: event/global/Ch1/CH_FakeJohn.sq:90
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"约翰大师，\n"
"{====}那本「幻想列车」是真的吗？\n"
"{====}没...没什么，我就是随便问问。"
msgstr ""
"约翰大师，\n"
"{====}那本「幻想列车」是真的吗？\n"
"{====}没...没什么，我就是随便问问。"

#: event/global/Ch1/CH_FakeJohn.sq:102
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"这家伙怎么又来了，\n"
"又要被他抢走饭碗了。"
msgstr ""
"这家伙怎么又来了，\n"
"又要被他抢走饭碗了。"

#: event/global/Ch1/CH_FakeJohn.sq:108
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"约...约翰大师，\n"
"{====}今...今天真早啊。"
msgstr ""
"约...约翰大师，\n"
"{====}今...今天真早啊。"

#: event/global/Ch1/CH_FakeJohn.sq:121
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"没...没什么，\n"
"{====}我没有说您坏话。"
msgstr ""
"没...没什么，\n"
"{====}我没有说您坏话。"

#: event/global/Ch1/CH_FakeJohn.sq:136
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"钥匙...钥匙...\n"
"{==}好像弄丢了。"
msgstr ""
"钥匙...钥匙...\n"
"{==}好像弄丢了。"

#: event/global/Ch1/CH_FakeJohn.sq:146
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"大师，西...西边，怪物在西边。\n"
"{==}我...我，我就不去了。"
msgstr ""
"大师，西...西边，怪物在西边。\n"
"{==}我...我，我就不去了。"

#: event/global/Ch1/CH_FakeJohn.sq:156
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"对对对，这是我的钥匙。\n"
"{==}您是在哪里找到的。"
msgstr ""
"对对对，这是我的钥匙。\n"
"{==}您是在哪里找到的。"

#: event/global/Ch1/CH_FakeJohn.sq:167
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"果然是大...大师，\n"
"{====}现在可以去炸蛞蝓的巢穴了。"
msgstr ""
"果然是大...大师，\n"
"{====}现在可以去炸蛞蝓的巢穴了。"

#: event/global/Ch1/CH_FakeJohn.sq:177
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"大师，\n"
"{====}还剩一个巢穴。"
msgstr ""
"大师，\n"
"{====}还剩一个巢穴。"

#: event/global/Ch1/CH_FakeJohn.sq:187
msgctxt "<talk> @CH_FakeJohn"
msgid ""
"大师，\n"
"{====}西面洞里的灯修好了.\n"
"{====}那里还有蛞蝓，你快去看看吧。这里发生了变化！"
msgstr ""
"大师，\n"
"{====}西面洞里的灯修好了.\n"
"{====}那里还有蛞蝓，你快去看看吧。这里发生了变化！"

#: event/global/Ch1/CH_FakeJohn.sq:177
msgctxt "<talk> @CH_FakeJohn"
msgid "unkown_addition"
msgstr ""
"这是一条测试的\n"
"{====}内容！！"
